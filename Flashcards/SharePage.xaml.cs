﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Flashcards.logic;
using Flashcards.logic.data;
using Flashcards.logic.data.types;
using Flashcards.net;

namespace Flashcards
{
    public partial class SharePage : IServerConnectorListener
    {
        private readonly Category _category;
        private int _loginActionId;
        private readonly Dictionary<int, UploadFileDescriptor> _toUploadImages;
        private readonly Dictionary<int, UploadFileResponseDescriptor> _responses;
        private IList<Flashcard> _flashcards;

        public SharePage()
        {
            InitializeComponent();
            _toUploadImages = new Dictionary<int, UploadFileDescriptor>();
            _responses = new Dictionary<int, UploadFileResponseDescriptor>();
            _category = GlobalState.SharingCategory;
            NameTextBox.Text = _category.Name;
        }

        private void ShareButtonClick(object sender, RoutedEventArgs e)
        {
            if (!Parse())
            {
                MessageBox.Show("wypełnij wszystkie pola");
                return;
            }
            IsEnabled = false;
            Progress.IsVisible = true;
            Share();
        }

        private void Share()
        {
            _toUploadImages.Clear();
             _flashcards = DbAdapter.FetchCategoryFlashcards(new DbContext(), _category);
            var flashcardsWithImages =
                _flashcards.Where(
                    f => f.FirstPageType == Flashcard.TypeBitmap || f.SecondPageType == Flashcard.TypeBitmap).ToList();
            var images = new List<UploadFileDescriptor>();
            foreach (var flashcard in flashcardsWithImages)
            {
                if (flashcard.FirstPageType == Flashcard.TypeBitmap)
                {
                    images.Add(new UploadFileDescriptor
                        {
                            Data = flashcard.FirstPageBitmap, 
                            FlashcardId = flashcard.FlashcardId,
                            FirstPage = true
                        });
                }
                if (flashcard.SecondPageType == Flashcard.TypeBitmap)
                {
                    images.Add(new UploadFileDescriptor
                    {
                        Data = flashcard.SecondPageBitmap,
                        FlashcardId = flashcard.FlashcardId,
                        FirstPage = false
                    });
                }
            }
            if(images.Count > 0)
            {
                foreach (var fileDescriptor in images)
                {
                    var actionId = GlobalState.ServerConnector.UploadImage(fileDescriptor.Data, new UploadFileData
                        {
                            Login = GlobalState.UserInfo.Login,
                            SessionId = GlobalState.UserInfo.Session
                        });
                    _toUploadImages.Add(actionId, fileDescriptor);
                }
            }
            else
            {
                GlobalState.ServerConnector.CreateCategory(new CreateCategoryData
                {
                    Login = GlobalState.UserInfo.Login,
                    SessionId = GlobalState.UserInfo.Session,
                    Category = new CategoryResponseData
                    {
                        Name = NameTextBox.Text,
                        Language = "pl"
                    },
                    Flashcards = CreateFlashcardsData(_flashcards),
                    Keywords = KeywordsTextBox.Text
                });
            }
        }

        private IList<FlashcardResponseData> CreateFlashcardsData(
            IEnumerable<Flashcard> flashcards)
        {
            return flashcards.Select(f => new FlashcardResponseData
                {
                    FirstPage = f.FirstPageType == Flashcard.TypeText ? f.FirstPageText : 
                        _responses[f.FlashcardId].FirstImage,

                    SecondPage = f.SecondPageType == Flashcard.TypeText ? f.SecondPageText : 
                     _responses[f.FlashcardId].SecondImage,

                    FirstPageType = f.FirstPageType == Flashcard.TypeText ? FlashcardResponseData.TypeText :
                        FlashcardResponseData.TypeImage,

                    SecondPageType = f.SecondPageType == Flashcard.TypeText ? FlashcardResponseData.TypeText : 
                        FlashcardResponseData.TypeImage
                }).ToList();
        }

        private bool Parse()
        {
            NameTextBox.Text = NameTextBox.Text.Trim();
            KeywordsTextBox.Text = KeywordsTextBox.Text.Trim();
            if (NameTextBox.Text.Length == 0 || KeywordsTextBox.Text.Length == 0)
            {
                return false;
            }
            return RetrieveKeywords().Count >= 4;
        }

        private IList<string> RetrieveKeywords()
        {
            return KeywordsTextBox.Text.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
        }

        public void ServerError(int actionId, ServerErrorType type)
        {
            switch(type)
            {
                case ServerErrorType.ConnectionError:
                    Dispatcher.BeginInvoke(() =>
                        {
                            Progress.IsVisible = false;
                            IsEnabled = true;
                            MessageBox.Show("wystąpił problem z połączeniem");
                        });
                    break;
                case ServerErrorType.SessionExpired:
                    _loginActionId = GlobalState.ServerConnector.Login(new LoginData
                        {
                            Login = GlobalState.UserInfo.Login,
                            Password = GlobalState.UserInfo.Password
                        });
                    break;
                case ServerErrorType.CategoryAlreadyExists:
                    Dispatcher.BeginInvoke(() =>
                    {
                        Progress.IsVisible = false;
                        IsEnabled = true;
                        MessageBox.Show("zestaw o takiej nazwie został już opublikowany. Zaproponuj inną nazwę");
                    });
                    break;
                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }

        public void ServerSuccess(int actionId, object response)
        {
            if(actionId == _loginActionId)
            {
                GlobalState.UserInfo.Session = response as string;
                Dispatcher.BeginInvoke(Share);
            }
            else if(_toUploadImages.ContainsKey(actionId))
            {
                var fileDescriptor = _toUploadImages[actionId];
                _toUploadImages.Remove(actionId);
                if(_responses.ContainsKey(fileDescriptor.FlashcardId))
                {
                    var item = _responses[fileDescriptor.FlashcardId];
                    Utils.Log("Add value to " + 
                        (fileDescriptor.FirstPage ? "First " : "Second ") + "Page of " + fileDescriptor.FlashcardId);
                    if(fileDescriptor.FirstPage)
                        item.FirstImage = response as string;
                    else
                        item.SecondImage = response as string;
                    _responses.Remove(fileDescriptor.FlashcardId);
                    _responses.Add(fileDescriptor.FlashcardId, item);
                }
                else
                {
                    _responses.Add(fileDescriptor.FlashcardId, new UploadFileResponseDescriptor
                    {
                        FirstImage = fileDescriptor.FirstPage ? response as string : null,
                        SecondImage = !fileDescriptor.FirstPage ? response as string : null
                    });
                    Utils.Log("Add value to " +
                        (fileDescriptor.FirstPage ? "First " : "Second ") + "Page of " + fileDescriptor.FlashcardId + "(create)");
                }

                foreach (var key in _responses.Keys)
                {
                    Utils.Log(key + ": " + _responses[key].FirstImage);
                    Utils.Log(key + ": " + _responses[key].SecondImage);
                }

                if(_toUploadImages.Count == 0)
                {
                    GlobalState.ServerConnector.CreateCategory(new CreateCategoryData
                    {
                        Login = GlobalState.UserInfo.Login,
                        SessionId = GlobalState.UserInfo.Session,
                        Category = new CategoryResponseData
                            {
                                Name = NameTextBox.Text,
                                Language = "pl"
                            },
                        Flashcards = CreateFlashcardsData(_flashcards),
                        Keywords = KeywordsTextBox.Text
                    });
                }
            }
            else
            {
                Dispatcher.BeginInvoke(() =>
                    {
                        Progress.IsVisible = false;
                        IsEnabled = true;
                        NavigationService.GoBack();
                    });
            }
        }
    }

    struct UploadFileDescriptor
    {
        public int FlashcardId { get; set; }
        public byte[] Data { get; set; }
        public bool FirstPage { get; set; }

    }

    struct UploadFileResponseDescriptor
    {
        public string FirstImage { get; set; }
        public string SecondImage { get; set; }
    }
}