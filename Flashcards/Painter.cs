﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Flashcards
{
    public class Painter
    {
        private readonly Image _image;
        private readonly TextBlock _textBlock;

        private WriteableBitmap _bitmap1;
        private WriteableBitmap _bitmap2;
        private bool _firstPage;
        private bool _showText1;
        private bool _showText2;
        private Size _size;
        private string _text1;
        private string _text2;

        public Painter(TextBlock textBlock, Image flashcardImage)
        {
            _textBlock = textBlock;
            _image = flashcardImage;
            _firstPage = true;
            _size = new Size();
        }

        public string Text1
        {
            get { return _text1; }
            set
            {
                _text1 = value;
                _showText1 = true;
            }
        }

        public string Text2
        {
            get { return _text2; }
            set
            {
                _text2 = value;
                _showText2 = true;
            }
        }

        public WriteableBitmap Bitmap1
        {
            get { return _bitmap1; }
            set
            {
                _bitmap1 = value;
                _showText1 = false;
            }
        }

        public WriteableBitmap Bitmap2
        {
            get { return _bitmap2; }
            set
            {
                _bitmap2 = value;
                _showText2 = false;
            }
        }

        public Size Size
        {
            get { return _size; }
            set
            {
                _size = value;
                Repaint();
            }
        }

        public void ChangePage()
        {
            _firstPage = !_firstPage;
            Repaint();
        }

        private void RepaintText()
        {
            _image.Visibility = Visibility.Collapsed;
            _textBlock.Visibility = Visibility.Visible;
            _textBlock.Text = _firstPage ? Text1 : Text2;
            _textBlock.FontSize = 48;
            if ((int)_size.Height == 0)
            {
                return;
            }
            _textBlock.Measure(_size);
// ReSharper disable CompareOfFloatsByEqualityOperator
            while (_textBlock.DesiredSize.Height == _size.Height)
// ReSharper restore CompareOfFloatsByEqualityOperator
            {
                _textBlock.FontSize -= 3;
                _textBlock.Measure(_size);
            }
        }

        private void RepaintBitmap()
        {
            _textBlock.Visibility = Visibility.Collapsed;
            _image.Visibility = Visibility.Visible;
            _image.Source = _firstPage ? _bitmap1 : _bitmap2;
        }

        public void Repaint()
        {
            if (_firstPage && _showText1 || !_firstPage && _showText2)
            {
                RepaintText();
            }
            else
            {
                RepaintBitmap();
            }
        }
    }
}