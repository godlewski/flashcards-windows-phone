﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Flashcards.net;

namespace Flashcards
{
    public partial class CommentsPage : IServerConnectorListener
    {
        private int _retrieveVotesActionId;
        private int _voteCategoryActionId;
        private int _loginActionId;
        private int _lastActionId;

        public CommentsPage()
        {
            InitializeComponent();
            RefreshList();
        }

        private void RefreshList()
        {
            Progress.IsVisible = true;
            _lastActionId = _retrieveVotesActionId = GlobalState.ServerConnector.RetrieveVotes(new RetrieveVotesData
                {
                    Login = GlobalState.UserInfo.Login,
                    SessionId = GlobalState.UserInfo.Session,
                    Id = GlobalState.SelectedCategory.ServerCategoryId
                });
        }

        public void ServerError(int actionId, ServerErrorType type)
        {
            switch (type)
            {
                case ServerErrorType.ConnectionError:
                    Dispatcher.BeginInvoke(() =>
                    {
                        Progress.IsVisible = false;
                        MessageBox.Show("błąd komunikacji");
                    });
                    break;
                case ServerErrorType.SessionExpired:
                    if (actionId == _loginActionId)
                    {
                        Dispatcher.BeginInvoke(() =>
                        {
                            Progress.IsVisible = false;
                            MessageBox.Show("nastąpił błąd");
                        });
                    }
                    else
                    {
                        _loginActionId = GlobalState.ServerConnector.Login(new LoginData
                        {
                            Login = GlobalState.UserInfo.Login,
                            Password = GlobalState.UserInfo.Password
                        });
                    }
                    break;
                default:
                    Dispatcher.BeginInvoke(() =>
                    {
                        Progress.IsVisible = false;
                        MessageBox.Show("nastąpił błąd");
                    });
                    break;
            }
        }

        public void ServerSuccess(int actionId, object response)
        {
            if(actionId == _loginActionId)
            {
                if(_lastActionId == _voteCategoryActionId)
                {
                    Dispatcher.BeginInvoke(() => PublishClick(null, null));
                }
                else
                {
                    Dispatcher.BeginInvoke(RefreshList);
                }
            }
            else if(actionId == _retrieveVotesActionId)
            {
                Dispatcher.BeginInvoke(() =>
                {
                    var votes = response as IList<VoteResponseData>;
                    if (votes == null) return;
                    var myVote = votes.FirstOrDefault(v => v.User == GlobalState.UserInfo.Login);
                    if (myVote != null)
                    {
                        CommentTextBox.Text = myVote.Comment;
                        Vote.Rating = myVote.Vote;
                    }
                    CommentsListBox.ItemsSource = new ObservableCollection<VoteResponseData>(votes);
                    Progress.IsVisible = false;
                });
            }
            else if(actionId == _voteCategoryActionId)
            {
                Dispatcher.BeginInvoke(() =>
                {
                    MessageBox.Show("opublikowano");
                    RefreshList();
                });
            }
        }

        private void PublishClick(object sender, RoutedEventArgs e)
        {
            _lastActionId = _voteCategoryActionId = GlobalState.ServerConnector.VoteCategory(new VoteCategoryData
                {
                    Login = GlobalState.UserInfo.Login,
                    SessionId = GlobalState.UserInfo.Session,
                    Comment = CommentTextBox.Text,
                    Id = GlobalState.SelectedCategory.ServerCategoryId,
                    Vote = Vote.Rating
                });
        }
    }
}