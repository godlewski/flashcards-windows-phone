﻿using System.Collections.Generic;
using Microsoft.Phone.Shell;

namespace Flashcards {

    public class ApplicationBarManager {

        private IApplicationBar bar;
        private List<IApplicationBarIconButton> buttons;
        private List<IApplicationBarMenuItem> items;
        private List<int> visibleButtonsIndices;
        private List<int> visibleItemsIndices;

        public ApplicationBarManager(IApplicationBar ApplicationBar) {
            bar = ApplicationBar;
            visibleButtonsIndices = new List<int>();
            buttons = new List<IApplicationBarIconButton>();
            int index = 0;
            foreach (IApplicationBarIconButton button in bar.Buttons) {
                buttons.Add(button);
                visibleButtonsIndices.Add(index++);
            }

            visibleItemsIndices = new List<int>();
            items = new List<IApplicationBarMenuItem>();
            index = 0;
            foreach (IApplicationBarMenuItem item in bar.MenuItems) {
                items.Add(item);
                visibleItemsIndices.Add(index++);
            }
        }

        public void HideButton(int[] indices) {
            bool change = false;
            foreach (int index in indices) {
                if (visibleButtonsIndices.Contains(index)) {
                    change = true;
                    visibleButtonsIndices.Remove(index);
                }
            }
            if (change) {
                RefreshButtons();
            }
        }


        public void ShowButton(int[] indices) {
            bool change = false;
            foreach (int index in indices) {
                if (!visibleButtonsIndices.Contains(index)) {
                    change = true;
                    visibleButtonsIndices.Add(index);
                }
            }
            if (change) {
                RefreshButtons();
            }
        }

        public void ShowButtons(int[] indicesToShow, int[] indicesToHide) {
            bool change = false;
            foreach (int index in indicesToShow) {
                if (!visibleButtonsIndices.Contains(index)) {
                    change = true;
                    visibleButtonsIndices.Add(index);
                }
            }
            foreach (int index in indicesToHide) {
                if (visibleButtonsIndices.Contains(index)) {
                    change = true;
                    visibleButtonsIndices.Remove(index);
                }
            }
            if (change) {
                RefreshButtons();
            }
        }

        private void RefreshButtons() {
            bar.Buttons.Clear();
            visibleButtonsIndices.Sort();
            foreach (int index in visibleButtonsIndices) {
                bar.Buttons.Add(buttons[index]);
            }
        }

        public void hideItem(int[] indices) {
            bool change = false;
            foreach (int index in indices) {
                if (!visibleItemsIndices.Contains(index)) {
                    change = true;
                    visibleItemsIndices.Remove(index);
                }
            }
            if (change) {
                refreshItems();
            }
        }


        public void showItem(int[] indices) {
            bool change = false;
            foreach (int index in indices) {
                if (!visibleItemsIndices.Contains(index)) {
                    change = true;
                    visibleItemsIndices.Add(index);
                }
            }
            if (change) {
                refreshItems();
            }
        }

        private void refreshItems() {
            bar.MenuItems.Clear();
            visibleItemsIndices.Sort();
            foreach (int index in visibleItemsIndices) {
                bar.MenuItems.Add(buttons[index]);
            }
        }
    }
}
