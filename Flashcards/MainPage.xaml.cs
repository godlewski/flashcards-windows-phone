﻿using System;
using System.ComponentModel;
using System.Windows;
using Flashcards.logic;
using Flashcards.logic.data.types;

namespace Flashcards
{
    public partial class MainPage : IAnimatorListener, AppLogic.IAppLogicListener
    {
        private readonly ApplicationBarManager _applicationBarManager;

        private readonly Flashcard _welcomeFlashcard = new Flashcard
            {
                Category = new Category { Name = "witaj" },
                FirstPageText = "Brak fiszek",
                SecondPageText = "Dodaj fiszki lub wybierz kategorie"
            };

        private Animator _animator;
        private bool _uiReady;
        private bool _logicReady;

        public MainPage()
        {
            InitializeComponent();
            _uiReady = _logicReady = false;
            AppLogic.Instance.Listener = this;

            _applicationBarManager = new ApplicationBarManager(ApplicationBar);
            _applicationBarManager.HideButton(new[] { 1, 2, 3 });
        }

        public Flashcard FetchNextFlashcard()
        {
            var flashcard = AppLogic.Instance.FetchNextFlashcard();
            if (flashcard == null)
            {
                flashcard = _welcomeFlashcard;
                _applicationBarManager.HideButton(new[] { 1, 2 });
            }
            else
            {
                _applicationBarManager.ShowButton(new[] { 1, 2 });
            }
            CategoryTextBlock.Text = flashcard.Category.Name;
            return flashcard;
        }

        public void StartHardWorking()
        {
            ProgressBar.Visibility = Visibility.Visible;
        }

        public void FinishedHardWorking()
        {
            ProgressBar.Visibility = Visibility.Collapsed;
        }

        public void UncheckedFlashcardsCount(int count)
        {
        }

        private void OnRectangleSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_animator == null)
            {
                _animator = new Animator(FlashcardContent, FlashcardTouchField, FlashcardText, FlashcardImage, this)
                    {
                        Dimension = new Size(FlashcardRectangle.ActualWidth, FlashcardRectangle.ActualHeight)
                    };
                _uiReady = true;
            }
            TryShowFlashcard();
        }

        private void TryShowFlashcard()
        {
            if (_uiReady && _logicReady)
            {
                _animator.LoadFlashcard();
            }
        }

        private void MenuCategoriesClick(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/CategoriesPage.xaml", UriKind.Relative));
        }

        private void MenuDownloadFlashcardsClick(object sender, EventArgs e)
        {
            if(GlobalState.IsLoggedIn)
            {
                NavigationService.Navigate(new Uri("/DownloadListPage.xaml", UriKind.Relative));
            }
            else
            {
                GlobalState.AfterLoginPage = "/DownloadListPage.xaml";
                NavigationService.Navigate(new Uri("/AuthorizationPage.xaml", UriKind.Relative));    
            }
        }

        private void MenuSettingsClick(object sender, EventArgs e)
        {
            // TODO: Add event handler implementation here.
        }

        private void MenuAddClick(object sender, EventArgs e)
        {
            GlobalState.Action = GlobalState.ActionType.AddFlashcard;
            NavigationService.Navigate(new Uri("/AddFlashcardPage.xaml", UriKind.Relative));
        }

        private void MenuEditClick(object sender, EventArgs e)
        {
            GlobalState.Action = GlobalState.ActionType.EditFlashcard;
            GlobalState.Flashcard = AppLogic.Instance.CurrentFlashcard;
            NavigationService.Navigate(new Uri("/AddFlashcardPage.xaml", UriKind.Relative));
        }

        private void MenuRemoveClick(object sender, EventArgs e)
        {
            if (MessageBox.Show("Fiszka zostanie usunięta", "Usuwanie fiszki", MessageBoxButton.OKCancel) !=
                MessageBoxResult.OK) return;
            AppLogic.Instance.RemoveFlashcard();
            _animator.LoadFlashcard();
        }

        private void MenuIntroduceClick(object sender, EventArgs e)
        {
            // TODO: Add event handler implementation here.
        }

        private void ButtonKnowClick(object sender, RoutedEventArgs e)
        {
            AppLogic.Instance.BasketUp();
            _animator.AnimateNext();
        }

        private void ButtonDontKnowClick(object sender, RoutedEventArgs e)
        {
            AppLogic.Instance.BasketDown();
            _animator.AnimateNext();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (GlobalState.ChangedSelection != null && GlobalState.ChangedSelection.Count > 0)
            {
                ProgressBar.Visibility = Visibility.Visible;
                var worker = new BackgroundWorker();
                worker.DoWork += (sender, args) => AppLogic.Instance.ChangedData();
                worker.RunWorkerCompleted += (sender, args) =>
                    {
                        ProgressBar.Visibility = Visibility.Collapsed;
                        _logicReady = true;
                        TryShowFlashcard();
                    };
                worker.RunWorkerAsync();
            }
            else
            {
                _logicReady = true;
                TryShowFlashcard();
            }
        }

    }
}