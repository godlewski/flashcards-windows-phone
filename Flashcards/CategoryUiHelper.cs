﻿
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Coding4Fun.Phone.Controls;
using Flashcards.logic;
using Flashcards.logic.data;
using Flashcards.logic.data.types;
using Microsoft.Phone.Controls;

namespace Flashcards
{
    public class CategoryUiHelper
    {
        public static void AddCategoryClick(ListBox allCategoriesListBox, ListBox ownCategoriesListBoxOrNull = null, bool selectAdded = true)
        {
            var prompt = new InputPrompt
            {
                Title = "Nowa kategoria",
                Message = "wprowadź nazwę kategorii",
                InputScope = new InputScope { Names = { new InputScopeName { NameValue = InputScopeNameValue.Text } } }
            };
            prompt.Completed += (senderObject, resultArgs) =>
                {
                    if (resultArgs.Result == null) return;
                    var result = resultArgs.Result.Trim();
                    if (result.Length <= 0) return;
                    try
                    {
                        var addedCategory = AppLogic.Instance.AddCategory(result);
                        var categories = allCategoriesListBox.ItemsSource as ObservableCollection<Category>;
                        if (categories != null) categories.Add(addedCategory);
                        if (selectAdded) allCategoriesListBox.SelectedIndex = allCategoriesListBox.Items.Count - 1;
                        if (ownCategoriesListBoxOrNull == null) return;
                        categories = ownCategoriesListBoxOrNull.ItemsSource as ObservableCollection<Category>;
                        if (categories != null) categories.Add(addedCategory);
                        if (selectAdded) ownCategoriesListBoxOrNull.SelectedIndex = allCategoriesListBox.Items.Count - 1;
                    }
                    catch (AlreadyExistsException)
                    {
                        MessageBox.Show("Kategoria o tej nazwie już istnieje");
                    }
                };
            prompt.Show();
        }

        public static void RemoveCategoryClick(MenuItem menuItem, params ObservableCollection<Category>[] categoryModels)
        {
            if (menuItem == null) return;
            var category = menuItem.DataContext as Category;
            if (MessageBox.Show("Kategoria wraz z jej fiszkami zostanie usunięta", "Usuwanie kategorii",
                                MessageBoxButton.OKCancel) != MessageBoxResult.OK) return;
            AppLogic.Instance.RemoveCategory(category);
            foreach (var collection in categoryModels)
            {
                collection.Remove(category);
            }
        }

        public static void RenameCategoryClick(MenuItem menuItem)
        {
            if (menuItem == null) return;
            var category = menuItem.DataContext as Category;
            if (category == null) return;
            var prompt = new InputPrompt
            {
                Title = "Nowa kategoria",
                Message = "wprowadź nazwę kategorii",
                Value = category.Name,
                InputScope = new InputScope { Names = { new InputScopeName { NameValue = InputScopeNameValue.Text } } }
            };
            prompt.Completed += (senderObject, resultArgs) =>
            {
                var result = resultArgs.Result.Trim();
                if (result.Length > 0)
                {
                    category.Name = result;
                }
                var db = new DbContext();
                var old = db.CategoriesTable.FirstOrDefault(c => c.CategoryId == category.CategoryId);
                if (old == null) return;
                old.Name = result;
                db.SubmitChanges();
            };
            prompt.Show();

        }
    }
}
