﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Flashcards.logic.data.types;
using ICSharpCode.SharpZipLib.Zip;

namespace Flashcards.logic.data
{
    public class DbAdapter
    {
        public static IEnumerable<Category> FetchAllCategories(DbContext db)
        {
            return db.CategoriesTable.ToList();
        }

        public static List<Category> FetchSelectedCategories(DbContext db)
        {
            return db.CategoriesTable.Where(c => c.IsSelected).ToList();
        }

        public static int FetchFlashcardsCount(DbContext db)
        {
            return db.FlashcardsTable.Count(f => f.Category.IsSelected);
        }

        public static bool CategoryExists(DbContext db, string name)
        {
            return db.CategoriesTable.Any(c => c.Name == name);
        }


        public static void AddCategory(DbContext db, Category category)
        {
            db.CategoriesTable.InsertOnSubmit(category);
            db.SubmitChanges();
        }

        public static void AddFlashcard(DbContext db, Flashcard flashcard)
        {
            flashcard.BasketNr = 1;
            flashcard.TimeToCheck = Utils.CurrentDate.AddHours(AppLogic.Baskets[1]);
            var cat = flashcard.Category ?? db.CategoriesTable.FirstOrDefault(c => c.CategoryId == flashcard.CatId);
            if (cat != null) cat.Count++;
            db.FlashcardsTable.InsertOnSubmit(flashcard);
            db.SubmitChanges();
        }

        public static PreparedListData FetchOrderedFlashcards(DbContext db, List<int> excluded)
        {
            excluded = new List<int>(excluded);
            var flashcards = FetchUncheckedsFlashcards(db, excluded, Config.BufferSize);
            excluded.AddRange(flashcards.Select(f => f.FlashcardId));
            if (flashcards.Count < Config.BufferSize)
            {
                flashcards.AddRange(FetchRandomFlashcards(db, excluded, Config.BufferSize - flashcards.Count));
            }
            return new PreparedListData
                {
                    NextCheckingTime = FetchNextCheckingTime(db, excluded),
                    PreparedList = flashcards
                };
        }

        private static List<Flashcard> FetchUncheckedsFlashcards(DbContext db, List<int> excluded, int limit)
        {
            var currentDate = Utils.CurrentDate;
            var result = (from f in db.FlashcardsTable
                          where f.Category.IsSelected
                                && currentDate >= f.TimeToCheck
                                && !excluded.Contains(f.FlashcardId)
                          orderby f.TimeToCheck
                          select f).Take(limit).ToList();
            db.SubmitChanges();
            return result;
        }

        private static IEnumerable<Flashcard> FetchRandomFlashcards(DbContext db, List<int> excluded, int limit)
        {
            db.SubmitChanges();
            var list = (from f in db.FlashcardsTable
                        where f.Category.IsSelected && !excluded.Contains(f.FlashcardId)
                        select f).ToList();
            db.SubmitChanges();
            Utils.Shuffle(list);
            return list.Take(limit).ToList();
        }

        private static DateTime FetchNextCheckingTime(DbContext db, List<int> excluded)
        {
            var currentDate = Utils.CurrentDate;
            var sel = from f in db.FlashcardsTable
                      where currentDate < f.TimeToCheck
                            && !excluded.Contains(f.FlashcardId)
                            && f.Category.IsSelected
                      orderby f.TimeToCheck
                      select f;
            var flashcard = sel.FirstOrDefault();
            return flashcard != null ? flashcard.TimeToCheck : DateTime.MaxValue;
        }

        public static void Remove(DbContext db, Flashcard flashcard)
        {
            db.FlashcardsTable.DeleteOnSubmit(
                flashcard = db.FlashcardsTable.FirstOrDefault(f => f.FlashcardId == flashcard.FlashcardId));
            if (flashcard != null) flashcard.Category.Count--;
            db.SubmitChanges();
        }

        public static void Remove(DbContext db, Category cat)
        {
            db.FlashcardsTable.DeleteAllOnSubmit(from f in db.FlashcardsTable
                                                 where f.Category.CategoryId == cat.CategoryId
                                                 select f);
            db.CategoriesTable.DeleteOnSubmit(db.CategoriesTable.FirstOrDefault(c => c.CategoryId == cat.CategoryId));
            db.SubmitChanges();
        }

        #region TESTY

        public static void TestAddCapitals()
        {
            var countries = new[]
                {
                    new[] {"Albania", "Tirana"},
                    new[] {"Andora", "Andora"},
                    new[] {"Austria", "Wiedeń"},
                    new[] {"Belgia", "Bruksela "},
                    new[] {"Białoruś", "Mińsk"},
                    new[] {"Bośnia i Hercegowina", "Sarajewo"},
                    new[] {"Bułgaria", "Sofia"},
                    new[] {"Chorwacja", "Zagrzeb"},
                    new[] {"Dania", "Kopenhaga"},
                    new[] {"Estonia", "Tallin"},
                    new[] {"Finlandia", "Helsinki"},
                    new[] {"Francja", "Paryż"},
                    new[] {"Grecja", "Ateny"},
                    new[] {"Hiszpania", "Madryt"},
                    new[] {"Holandia", "Amsterdam"},
                    new[] {"Irlandia", "Dublin"},
                    new[] {"Islandia", "Reykiavik"},
                    new[] {"Serbia i Czarnogóra", "Belgrad"},
                    new[] {"Lichtenstein", "Vaduz"},
                    new[] {"Litwa", "Wilno"},
                    new[] {"Luksemburg", "Luksemburg"},
                    new[] {"Łotwa", "Ryga"},
                    new[] {"Macedonia", "Skopie"},
                    new[] {"Malta", "Valletta"},
                    new[] {"Mołdawia", "Kiszyniów"},
                    new[] {"Monako", "Monako"},
                    new[] {"Niemcy", "Berlin"},
                    new[] {"Norwegia", "Oslo"},
                    new[] {"Polska", "Warszawa"},
                    new[] {"Portugalia", "Lizbona"},
                    new[] {"Republika Czeska", "Praga"},
                    new[] {"Rosja", "Moskwa"},
                    new[] {"Rumunia", "Bukareszt"},
                    new[] {"San Marino", "San Marino"},
                    new[] {"Słowacja", "Bratysława"},
                    new[] {"Słowenia", "Lublana"},
                    new[] {"Szwajcaria", "Berno"},
                    new[] {"Szwecja", "Sztokholm"},
                    new[] {"Ukraina", "Kijów"},
                    new[] {"Watykan", "Watykan"},
                    new[] {"Węgry", "Budapeszt"},
                    new[] {"Wielka Brytania", "Londyn"},
                    new[] {"Włochy", "Rzym"}
                };
            var category = new Category
                {
                    Name = "Stolice europejskie",
                    IsSelected = true,
                    Published = false,
                    Downloaded = false
                };
            var db = new DbContext();
            AddCategory(db, category);
            foreach (var f in countries.Select(pair => new Flashcard
                {
                    FirstPageText = pair[0],
                    SecondPageText = pair[1],
                    Category = category
                }))
            {
                AddFlashcard(db, f);
            }
        }

        public static void TestAddFlags()
        {
            var targetUri = new Uri("http://dl.dropbox.com/u/533849/flags.zip");
            var request = (HttpWebRequest) WebRequest.Create(targetUri);
            request.BeginGetResponse(OnDownloadedFile, request);
        }

        private static void OnDownloadedFile(IAsyncResult callbackResult)
        {
            var files = new[]
                {
                    "Albania.png", "Andorra.png",
                    "Austria.png", "Belarus.png", "Belgium.png",
                    "Bosnia and Herzegovina.png", "Bulgaria.png", "Croatia.png",
                    "Cyprus.png", "Czech Republic.png", "Denmark.png",
                    "Estonia.png", "Finland.png", "France.png", "Georgia.png",
                    "Germany.png", "Greece.png", "Hungary.png", "Iceland.png",
                    "Ireland.png", "Italy.png", "latvia.png", "Liechtenstein.png",
                    "Lithuania.png", "Luxembourg.png", "Macedonia.png",
                    "Malta.png", "Monaco.png", "Montenegro.png", "Nederland.png",
                    "Norway.png", "Poland.png", "Portugal.png",
                    "Republic of Moldova.png", "Romania.png", "Russia.png",
                    "San Marino.png", "Serbia.png", "Slovakia.png", "Slovenia.png",
                    "Spain.png", "Sweden.png", "Switzerland.png", "Turkey.png",
                    "Ukraine.png", "United Kingdom.png", "Vatican.png"
                };
            var names = new[]
                {
                    "Albania", "Andora", "Austria",
                    "Białoruś", "Belgia", "Bośnia i Hercegowina", "Bułgaria",
                    "Chorwacja", "Cypr", "Czechy", "Dania", "Estonia", "Finlandia",
                    "Francja", "Gruzja", "Niemcy", "Grecja", "Węgry", "Islandia",
                    "Irlandia", "Włochy", "Łotwa", "Liechtenstein", "Litwa",
                    "Luksemburg", "Macedonia", "Malta", "Monaco", "Czarnogóra",
                    "Nederland", "Norwegia", "Polska", "Portugalia",
                    "Republika Mołdowy", "Rumunia", "Rosja", "San Marino",
                    "Serbia", "Słowacja", "Słowenia", "Hiszpania", "Szwecja",
                    "Szwajcaria", "Turcja", "Ukraina", "Zjednoczone Królestwo",
                    "Watykan"
                };
            var map = new Dictionary<string, string>();
            for (var i = 0; i < files.Length; i++)
            {
                map.Add(files[i], names[i]);
            }
            var myRequest = (HttpWebRequest) callbackResult.AsyncState;
            var myResponse = (HttpWebResponse) myRequest.EndGetResponse(callbackResult);
            using (var httpwebStreamReader = new StreamReader(myResponse.GetResponseStream()))
            {
                using (var input = new ZipInputStream(httpwebStreamReader.BaseStream))
                {
                    var category = new Category
                        {
                            Name = "Flagi europejskie",
                            IsSelected = true,
                            Published = false,
                            Downloaded = false
                        };
                    var db = new DbContext();
                    AddCategory(db, category);
                    while (true)
                    {
                        var entry = input.GetNextEntry();
                        if (entry == null)
                        {
                            break;
                        }
                        var buf = new byte[(int) entry.Size];
                        int c;
                        var size = 0;
                        while ((c = input.ReadByte()) != -1)
                        {
                            buf[size++] = (byte) c;
                        }
                        var f = new Flashcard
                            {
                                FirstPageBitmap = buf,
                                SecondPageText = map[entry.Name],
                                Category = category
                            };
                        AddFlashcard(db, f);
                    }
                    input.Close();
                    httpwebStreamReader.Close();
                }
            }
        }

        #endregion

        public static UserInfo FetchUserInfo()
        {
            return new DbContext().UserInfoTable.FirstOrDefault();
        }

        public static void SetUserInfo(UserInfo value)
        {
            var db = new DbContext();
            var item = db.UserInfoTable.FirstOrDefault();
            if (item != null)
            {
                db.UserInfoTable.DeleteOnSubmit(item);
            }
            db.UserInfoTable.InsertOnSubmit(value);
            db.SubmitChanges();
        }

        public static IList<Flashcard> FetchCategoryFlashcards(DbContext db, Category category)
        {
            return (from f in db.FlashcardsTable 
                    where f.Category.CategoryId == category.CategoryId 
                    select f).ToList();
        }
    }
}