﻿using System.ComponentModel;
using System.Data.Linq.Mapping;

namespace Flashcards.logic.data.types
{

    [Table]
    public class Category : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _name;
        private bool _selected;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int CategoryId { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name == value) return;
                _name = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public int ServerCategoryId { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public int Count { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public bool IsSelected
        {
            get { return _selected; }
            set
            {
                if (_selected == value) return;
                _selected = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsSelected"));
                }
            }
        }

        [Column(UpdateCheck = UpdateCheck.Never, CanBeNull = true)]
        public string Author { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public bool Downloaded { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public bool Published { get; set; }

    }
}
