﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flashcards.logic.data.types;

namespace Flashcards.logic {
    class ModificationDateComparator : IComparer<Flashcard> {

        public int Compare(Flashcard f1, Flashcard f2) {
            return DateTime.Compare(f1.TimeToCheck, f2.TimeToCheck);
        }
    }
}
