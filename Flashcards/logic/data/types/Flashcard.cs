﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Flashcards.logic.data.types
{

    [Table]
    public class Flashcard
    {
        public const int TypeText = 1;
        public const int TypeBitmap = 2;

        private EntityRef<Category> _categoryRef;
        private byte[] _firstPageBitmap;
        private byte[] _secondPageBitmap;
        private string _firstPageText;
        private string _secondPageText;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int FlashcardId { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public DateTime TimeToCheck { get; set; }

        [Column(CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
        public int Type { get; set; }

        public Flashcard(Flashcard flashcard)
        {
            FirstPageBitmap = flashcard.FirstPageBitmap;
            SecondPageBitmap = flashcard.SecondPageBitmap;
            FirstPageType = flashcard.FirstPageType;
            SecondPageType = flashcard.SecondPageType;
            BasketNr = flashcard.BasketNr;
            Category = flashcard.Category;
            CatId = flashcard.CatId;
            TimeToCheck = flashcard.TimeToCheck;
        }

        public Flashcard()
        {
        }

        [Column(DbType = "image", UpdateCheck = UpdateCheck.Never, CanBeNull = true)]
        public byte[] FirstPageBitmap
        {
            get { return _firstPageBitmap; }
            set
            {
                if (value != null)
                {
                    FirstPageText = null;
                    FirstPageType = TypeBitmap;
                }
                _firstPageBitmap = value;
            }
        }

        [Column(CanBeNull = true, UpdateCheck = UpdateCheck.Never)]
        public string FirstPageText
        {
            get { return _firstPageText; }
            set
            {
                if (value != null && value.Trim().Length > 0)
                {
                    FirstPageType = TypeText;
                    FirstPageBitmap = null;
                }
                _firstPageText = value;
            }
        }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public int FirstPageType { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public int SecondPageType { get; set; }

        [Column(DbType = "image", UpdateCheck = UpdateCheck.Never, CanBeNull = true)]
        public byte[] SecondPageBitmap
        {
            get { return _secondPageBitmap; }
            set
            {
                if (value != null)
                {
                    SecondPageText = null;
                    SecondPageType = TypeBitmap;
                }
                _secondPageBitmap = value;
            }
        }

        [Column(CanBeNull = true, UpdateCheck = UpdateCheck.Never)]
        public string SecondPageText
        {
            get { return _secondPageText; }
            set
            {
                if (value != null && value.Trim().Length > 0)
                {
                    SecondPageType = TypeText;
                    SecondPageBitmap = null;
                }
                _secondPageText = value;
            }
        }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public int BasketNr { get; set; }

        [Column(UpdateCheck = UpdateCheck.Never)]
        public int CatId { get; set; }

        [Association(ThisKey = "CatId", OtherKey = "CategoryId", IsForeignKey = true, Storage = "_categoryRef")]
        public Category Category
        {
            get
            {
                return _categoryRef.Entity;
            }
            set
            {
                if (_categoryRef.Entity == value && _categoryRef.HasLoadedOrAssignedValue) return;
                _categoryRef.Entity = value;
                if (value != null) CatId = value.CategoryId;
            }
        }
    }
}
