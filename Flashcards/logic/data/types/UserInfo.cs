﻿using System.Data.Linq.Mapping;

namespace Flashcards.logic.data.types
{
    [Table]
    public class UserInfo
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int UserInfoId { get; set; }

        [Column]
        public string Login { get; set; }

        [Column]
        public string Password { get; set; }

        [Column]
        public string Session { get; set; }
    }
}
