﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flashcards.logic.data.types;

namespace Flashcards.logic
{
    public class PreparedListData
    {

        public List<Flashcard> PreparedList { get; set; }

        public DateTime NextCheckingTime { get; set; }
    }
}
