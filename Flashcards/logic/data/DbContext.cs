﻿using System.Data.Linq;
using Flashcards.logic.data.types;

namespace Flashcards.logic.data
{
    public class DbContext : DataContext
    {
        public DbContext()
            : base(Config.DbConnection)
        {
            if (!DatabaseExists())
            {
                CreateDatabase();
            }
            //Log = new LogLinq2Sql();
        }

        internal Table<Category> CategoriesTable
        {
            get { return GetTable<Category>(); }
        }

        internal Table<Flashcard> FlashcardsTable
        {
            get { return GetTable<Flashcard>(); }
        }

        internal Table<UserInfo> UserInfoTable
        {
            get { return GetTable<UserInfo>(); }
        }

    }
}