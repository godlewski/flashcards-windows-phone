﻿namespace Flashcards.logic
{
    public class Config
    {
        public const string DbConnection = "Data Source=isostore:/flashcards_db.sdf";

        public const int BufferSize = 10;
        public const int BufferSizeMin = 5;
    }
}