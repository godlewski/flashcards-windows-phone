﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Flashcards.logic.data;
using Flashcards.logic.data.types;

namespace Flashcards.logic
{
    public class AppLogic
    {
        public static Dictionary<int, int> Baskets;
        public static readonly AppLogic Instance = new AppLogic();
        private readonly List<Flashcard> _bufferList;

        private readonly object _monitor = new object();
        private readonly List<Flashcard> _preparedList;
        private readonly Dictionary<int, Category> _selectedCategories;
        private int _count;
        private bool _grabbing;
        private DateTime _nextCheckingTime;
        private int _uncheckedCount;

        private AppLogic()
        {
            Baskets = new Dictionary<int, int>();
            _selectedCategories = new Dictionary<int, Category>();
            _preparedList = new List<Flashcard>();
            _bufferList = new List<Flashcard>();
            _grabbing = false;
            Baskets.Add(1, 12);
            Baskets.Add(2, 24);
            Baskets.Add(3, 48);
            Baskets.Add(4, 72);
            Baskets.Add(5, 168);
            Restart();
        }

        private void Restart()
        {
            var db = new DbContext();
            _selectedCategories.Clear();
            foreach (var category in DbAdapter.FetchSelectedCategories(db))
            {
                _selectedCategories.Add(category.CategoryId, category);
            }
            _count = DbAdapter.FetchFlashcardsCount(db);
            var data = DbAdapter.FetchOrderedFlashcards(db, new List<int>());
            _preparedList.Clear();
            _bufferList.Clear();
            _preparedList.AddRange(data.PreparedList);
            CalculateUncheckeds();

            if (_count > Config.BufferSize)
            {
                Grab();
            }
        }

        public IAppLogicListener Listener { set; private get; }

        public Flashcard CurrentFlashcard { get; private set; }

        private void CalculateUncheckeds()
        {
            lock (_monitor)
            {
                var count = 0;
                var dt = Utils.CurrentDate;
                foreach (var f in _preparedList)
                {
                    if (f.TimeToCheck < dt)
                    {
                        count++;
                    }
                    else
                    {
                        break;
                    }
                }
                _uncheckedCount = count;
            }
        }

        public Flashcard FetchNextFlashcard()
        {
            if (_count == 0)
            {
                return null;
            }

            if (_count <= Config.BufferSize)
            {
                if (_preparedList.Count == 0)
                {
                    RecalculateLocalFlashcards();
                }
                return PollPreparedFlashcard();
            }

            lock (_monitor)
            {
                if (_preparedList.Count == 0)
                {
                    Listener.StartHardWorking();
                    Monitor.Wait(_monitor);
                    Listener.FinishedHardWorking();
                }
                else
                {
                    if (_preparedList.Count < Config.BufferSizeMin || DateTime.Now >= _nextCheckingTime)
                    {
                        Grab();
                    }
                }
                return PollPreparedFlashcard();
            }
        }

        public void Grab()
        {
            lock (_monitor)
            {
                if (_grabbing)
                {
                    return;
                }
                _grabbing = true;
                ThreadPool.QueueUserWorkItem(DoGrab);
            }
        }

        private void DoGrab(object param)
        {
            var db = new DbContext();
            var ids = new List<int>();
            lock (_monitor)
            {
                ids.AddRange(_preparedList.Select(f => f.FlashcardId));
                ids.AddRange(_bufferList.Select(f => f.FlashcardId));
            }

            var data = DbAdapter.FetchOrderedFlashcards(db, ids);
            _nextCheckingTime = data.NextCheckingTime;

            lock (_monitor)
            {
                _preparedList.AddRange(data.PreparedList);
                RecalculateLocalFlashcards();
                while (_preparedList.Count > Config.BufferSize)
                {
                    _preparedList.RemoveAt(_preparedList.Count - 1);
                }
                Monitor.Pulse(_monitor);
                _grabbing = false;
            }
        }

        private Flashcard PollPreparedFlashcard()
        {
            Flashcard flashcard;
            lock (_monitor)
            {
                flashcard = RemoveHead(_preparedList);
            }
            flashcard.TimeToCheck = Utils.CurrentDate.AddHours(Baskets[flashcard.BasketNr]);
            var db = new DbContext();
            var old = db.FlashcardsTable.FirstOrDefault(f => f.FlashcardId == flashcard.FlashcardId);
            if (old != null)
                old.TimeToCheck = flashcard.TimeToCheck;
            db.SubmitChanges();
            Listener.UncheckedFlashcardsCount(_uncheckedCount);
            _uncheckedCount = Math.Max(0, _uncheckedCount - 1);
            lock (_monitor)
            {
                _bufferList.Add(flashcard);
            }
            CurrentFlashcard = flashcard;
            return flashcard;
        }

        private static Flashcard RemoveHead(IList<Flashcard> list)
        {
            var f = list[0];
            list.RemoveAt(0);
            return f;
        }

        private void RecalculateLocalFlashcards()
        {
            var shuffled = new List<Flashcard>();
            var currentDate = Utils.CurrentDate;
            lock (_monitor)
            {
                _preparedList.AddRange(_bufferList);
                _bufferList.Clear();
                _preparedList.Sort(new ModificationDateComparator());
                for (var i = _preparedList.Count - 1; i >= 0; i--)
                {
                    var f = _preparedList[i];
                    if (f.TimeToCheck <= currentDate)
                    {
                        break;
                    }
                    shuffled.Add(f);
                    _preparedList.RemoveAt(i);
                }
                Utils.Shuffle(shuffled);
                _preparedList.AddRange(shuffled);
                CalculateUncheckeds();
            }
        }

        public Category AddCategory(string name)
        {
            var db = new DbContext();
            if (DbAdapter.CategoryExists(db, name))
            {
                throw new AlreadyExistsException();
            }
            var category = new Category { Count = 0, IsSelected = true, Name = name, Downloaded = false };
            DbAdapter.AddCategory(db, category);
            _selectedCategories.Add(category.CategoryId, category);
            return category;
        }

        public void AddFlashcard(Flashcard flashcard)
        {
            DbAdapter.AddFlashcard(new DbContext(), flashcard);
            if (!flashcard.Category.IsSelected) return;
            if (!_selectedCategories.ContainsKey(flashcard.Category.CategoryId))
            {
                _selectedCategories.Add(flashcard.Category.CategoryId, flashcard.Category);
            }
            var previousCount = _count;
            _count++;
            if (_count > Config.BufferSize && previousCount == Config.BufferSize)
            {
                Grab();
            }
            else if (_count <= Config.BufferSize)
            {
                lock (_monitor)
                {
                    _bufferList.Add(flashcard);
                }
            }
        }

        public void RemoveFlashcard()
        {
            if (CurrentFlashcard == null) return;
            lock (_monitor)
            {
                _preparedList.Remove(CurrentFlashcard);
                _bufferList.Remove(CurrentFlashcard);
            }
            DbAdapter.Remove(new DbContext(), CurrentFlashcard);
            _count--;
        }

        public void RemoveCategory(Category category)
        {
            lock (_monitor)
            {
                for (var i = _preparedList.Count - 1; i >= 0; i--)
                {
                    if (_preparedList[i].Category.CategoryId != category.CategoryId) continue;
                    _preparedList.RemoveAt(i);
                }

                for (var i = _bufferList.Count - 1; i >= 0; i--)
                {
                    if (_bufferList[i].Category.CategoryId != category.CategoryId) continue;
                    _bufferList.RemoveAt(i);
                }
            }
            var db = new DbContext();
            _count = DbAdapter.FetchFlashcardsCount(db);
            _selectedCategories.Remove(category.CategoryId);
            DbAdapter.Remove(db, category);
        }


        public void BasketUp()
        {
            if (CurrentFlashcard.BasketNr >= Baskets.Count) return;
            var db = new DbContext();
            db.FlashcardsTable.Attach(CurrentFlashcard);
            CurrentFlashcard.BasketNr++;
            db.SubmitChanges();
        }

        public void BasketDown()
        {
            if (CurrentFlashcard.BasketNr <= 1) return;
            var db = new DbContext();
            db.FlashcardsTable.Attach(CurrentFlashcard);
            CurrentFlashcard.BasketNr--;
            db.SubmitChanges();
        }

        public interface IAppLogicListener
        {
            void StartHardWorking();
            void FinishedHardWorking();
            void UncheckedFlashcardsCount(int count);
        }

        public void ChangedData()
        {
            Restart();
        }

        public void EditFlashcard(Flashcard flashcard)
        {
            var db = new DbContext();
            var old = db.FlashcardsTable.FirstOrDefault(f => f.FlashcardId == flashcard.FlashcardId);
            if (old == null) return;
            old.FirstPageBitmap = flashcard.FirstPageBitmap;
            old.FirstPageText = flashcard.FirstPageText;
            old.FirstPageType = flashcard.FirstPageType;
            old.SecondPageBitmap = flashcard.SecondPageBitmap;
            old.SecondPageText = flashcard.SecondPageText;
            old.SecondPageType = flashcard.SecondPageType;
            db.SubmitChanges();
        }
    }
}