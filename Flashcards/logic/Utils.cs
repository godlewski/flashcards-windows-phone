﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Media.Imaging;
using Microsoft.Phone;

namespace Flashcards.logic {
    public class Utils {
        private static readonly DateTime Jan1St1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long CurrentTimeMillis() {
            return (long) (DateTime.UtcNow - Jan1St1970).TotalMilliseconds;
        }

        internal static DateTime CurrentDate {
            get {
                var time = DateTime.Now;
                return time.AddMilliseconds(-time.Millisecond).AddSeconds(-time.Second);
            }
        }

        public static void Log(string text) {
            System.Diagnostics.Debug.WriteLine(text);
        }


        public static void Shuffle<T>(List<T> items) {
            Random rnd = new Random();
            for (var i = 1; i < items.Count; i++) {
                var pos = rnd.Next(i + 1);
                var x = items[i];
                items[i] = items[pos];
                items[pos] = x;
            }
        }

        public static WriteableBitmap DecodeBitmap(Stream stream) {
            return PictureDecoder.DecodeJpeg(stream, 500, 500);
        }

        public static WriteableBitmap DecodeBitmap(byte[] bytes) {
            return DecodeBitmap(new MemoryStream(bytes));
        }
    }
}
