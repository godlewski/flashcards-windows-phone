﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Flashcards.logic.data;
using Flashcards.logic.data.types;
using Flashcards.net;

namespace Flashcards
{
    public class GlobalState
    {
        private static GlobalServerConnectorListener _globalServerListener;
        private static UserInfo _uInfo;

        public enum ActionType
        {
            AddFlashcard,
            EditFlashcard
        }

        public static ActionType Action { get; set; }
        public static Flashcard Flashcard { get; set; }
        public static List<int> ChangedSelection { get; set; }

        public static UserInfo UserInfo
        {
            get
            {
                if (_uInfo != null)
                {
                    return _uInfo;
                }
                _uInfo = DbAdapter.FetchUserInfo() ?? new UserInfo();
                return _uInfo;
            }

            set
            {
                _uInfo = value;
                DbAdapter.SetUserInfo(value);
            }
        }

        public static bool IsLoggedIn
        {
            get
            {
                var uInfo = UserInfo;
                return uInfo != null && uInfo.Session != null && uInfo.Session.Length > 0;
            }
        }

        public static string AfterLoginPage { get; set; }
        public static ServerConnector ServerConnector { get; set; }

        public static IServerConnectorListener ServerConnectorListener
        {
            get { return _globalServerListener ?? (_globalServerListener = new GlobalServerConnectorListener()); }
        }

        public static Category SharingCategory { get; set; }

        public static Category SelectedCategory { get; set; }
    }

    internal class GlobalServerConnectorListener : IServerConnectorListener
    {
        public void ServerError(int actionId, ServerErrorType type)
        {
            ((App) Application.Current).RootFrame.Dispatcher.BeginInvoke(() =>
                {
                    var currentPage = ((App) Application.Current).RootFrame.Content as Page;
                    if (!(currentPage is IServerConnectorListener)) return;
                    var listener = currentPage as IServerConnectorListener;
                    listener.ServerError(actionId, type);
                });
        }

        public void ServerSuccess(int actionId, object response)
        {
            ((App) Application.Current).RootFrame.Dispatcher.BeginInvoke(() =>
                {
                    var currentPage = ((App) Application.Current).RootFrame.Content as Page;
                    if (!(currentPage is IServerConnectorListener)) return;
                    var listener = currentPage as IServerConnectorListener;
                    listener.ServerSuccess(actionId, response);
                });
        }
    }
}