﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Navigation;
using Flashcards.logic.data;
using Flashcards.logic.data.types;
using Microsoft.Phone.Controls;

namespace Flashcards
{
    public partial class CategoriesPage
    {
        private const string TagAll = "All";
        private const string TagDownloaded = "Downloaded";
        private const string TagOwn = "Own";

        private readonly ApplicationBarManager _appBarMgr;
        private readonly ObservableCollection<Category> _allCategories;
        private readonly ObservableCollection<Category> _ownCategories;
        private readonly ObservableCollection<Category> _downloadedCategories;
        private readonly Dictionary<int, object> _changes;
        private string _currentPivot;
        private readonly DbContext _db;

        public CategoriesPage()
        {
            InitializeComponent();
            GlobalState.ChangedSelection = null;
            _changes = new Dictionary<int, object>();
            _currentPivot = TagAll;
            _appBarMgr = new ApplicationBarManager(ApplicationBar);
            _allCategories = new ObservableCollection<Category>((_db = new DbContext()).CategoriesTable.ToList());
            foreach (var category in _allCategories)
            {
                category.PropertyChanged += CategoryChanged;
            }
            _ownCategories = new ObservableCollection<Category>(_allCategories.Where(c => !c.Downloaded));
            _downloadedCategories = new ObservableCollection<Category>(_allCategories.Where(c => c.Downloaded));
            AllList.ItemsSource = _allCategories;
            DownloadedList.ItemsSource = _downloadedCategories;
            OwnsList.ItemsSource = _ownCategories;
        }

        private void CategoryChanged(object sender, PropertyChangedEventArgs e)
        {
            _db.SubmitChanges();
            var category = sender as Category;
            if (category == null) return;
            if (_changes.ContainsKey(category.CategoryId))
                _changes.Remove(category.CategoryId);
            else _changes.Add(category.CategoryId, true);
        }

        private void DownloadClick(object sender, EventArgs e)
        {
            // TODO: Add event handler implementation here.
        }

        private void SelectAllClick(object sender, EventArgs e)
        {
            foreach (var c in CurrentCategories)
                c.IsSelected = true;
        }

        private void UnselectAllClick(object sender, EventArgs e)
        {
            foreach (var c in CurrentCategories)
                c.IsSelected = false;
        }

        private void LoadedPivotItem(object sender, PivotItemEventArgs e)
        {
            _currentPivot = e.Item.Tag as string;
            if (e.Item.Tag.Equals(TagDownloaded))
            {
                _appBarMgr.ShowButtons(new[] {1}, new[] {0});
            }
            else
            {
                _appBarMgr.ShowButtons(new[] {0}, new[] {1});
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs navigatingCancelEventArgs)
        {
            GlobalState.ChangedSelection = _changes.Keys.ToList();
        }

        private IEnumerable<Category> CurrentCategories
        {
            get
            {
                return _currentPivot == TagAll
                           ? _allCategories
                           : _currentPivot == TagOwn
                                 ? _ownCategories
                                 : _downloadedCategories;
            }
        }

        private void RenameCategoryClick(object sender, RoutedEventArgs e)
        {
            CategoryUiHelper.RenameCategoryClick(sender as MenuItem);
        }

        private void RemoveCategoryClick(object sender, RoutedEventArgs e)
        {
            CategoryUiHelper.RemoveCategoryClick(sender as MenuItem, _allCategories, _ownCategories,
                                                 _downloadedCategories);
        }

        private void AddCategoryClick(object sender, EventArgs e)
        {
            CategoryUiHelper.AddCategoryClick(AllList, OwnsList, false);
        }

        private void ShareCategoryClick(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            if (menuItem == null) return;
            GlobalState.SharingCategory = menuItem.DataContext as Category;
            if (GlobalState.IsLoggedIn)
            {
                NavigationService.Navigate(new Uri("/SharePage.xaml", UriKind.Relative));
            }
            else
            {
                GlobalState.AfterLoginPage = "/SharePage.xaml";
                NavigationService.Navigate(new Uri("/AuthorizationPage.xaml", UriKind.Relative));
            }
        }
    }
}