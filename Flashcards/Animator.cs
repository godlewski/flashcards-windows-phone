﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Flashcards.logic;
using Flashcards.logic.data.types;
using Microsoft.Phone;

namespace Flashcards
{
    public class Animator
    {
        private const double RotationScale = 0.45;
        private readonly int _height;
        private readonly IAnimatorListener _listener;
        private readonly Painter _painter;

        private readonly PlaneProjection _projection;
        private readonly CompositeTransform _transform;
        private int _deltaCounter;
        private double _dx;
        private double _dy;
        private bool _horizontalBlocked;
        private double _originalAngle;
        private bool _verticalBlocked;

        public Animator(Panel flashcard, UIElement touchField, TextBlock textBlock, Image flashcardImage,
                        IAnimatorListener listener)
        {
            _painter = new Painter(textBlock, flashcardImage);
            _listener = listener;
            _projection = new PlaneProjection();
            flashcard.Projection = _projection;
            _transform = new CompositeTransform {ScaleX = 1};
            foreach (var child in flashcard.Children)
            {
                child.RenderTransform = _transform;
            }
            touchField.ManipulationStarted += ManipulationStarted;
            touchField.ManipulationDelta += ManipulationDelta;
            touchField.ManipulationCompleted += ManipulationCompleted;
            _height = (int)Application.Current.Host.Content.ActualHeight;
        }

        public Size Dimension
        {
            get { return _painter.Size; }
            set { _painter.Size = value; }
        }

        private void ManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            _horizontalBlocked = _verticalBlocked = false;
            _deltaCounter = 3;
            _dx = _dy = 0;
            _projection.RotationY = NormalizedAngle(_projection.RotationY);
            _originalAngle = _projection.RotationY;
        }

        private static double NormalizedAngle(double angle)
        {
            while (angle < 0)
            {
                angle += 360;
            }
            return angle % 360;
        }

        private void ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (_deltaCounter > 0)
            {
                _dx += Math.Abs(e.DeltaManipulation.Translation.X);
                _dy += Math.Abs(e.DeltaManipulation.Translation.Y);
                if (--_deltaCounter == 0)
                {
                    if (_dx >= _dy)
                    {
                        _verticalBlocked = true;
                        _projection.LocalOffsetY = 0;
                    }
                    else
                    {
                        _horizontalBlocked = true;
                        _projection.RotationY = _originalAngle;
                    }
                }
            }
            if (!_horizontalBlocked)
            {
                int angle = (int)(_projection.RotationY - e.DeltaManipulation.Translation.X * RotationScale) % 360;
                UpdateAngle(angle);
                _projection.RotationY = angle;
            }
            if (!_verticalBlocked)
            {
                _projection.LocalOffsetY += e.DeltaManipulation.Translation.Y;
            }
        }

        private void UpdateAngle(int angle)
        {
            if (angle < 0)
            {
                angle += 360;
            }
            if (angle > 90 && angle < 270 && _transform.ScaleX != -1)
            {
                _transform.ScaleX = -1;
                _painter.ChangePage();
            }
            else if ((angle < 90 || angle > 270) && _transform.ScaleX != 1)
            {
                _transform.ScaleX = 1;
                _painter.ChangePage();
            }
        }

        private void ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            var angle = (int)_projection.RotationY;
            double destinationAngle;
            if (angle < -90)
            {
                destinationAngle = -180;
            }
            else if (angle < 90)
            {
                destinationAngle = 0;
            }
            else if (angle < 270)
            {
                destinationAngle = 180;
            }
            else
            {
                destinationAngle = 360;
            }
            CreateStoryboard(
                "RotationY",
                angle,
                destinationAngle,
                Math.Abs(angle - destinationAngle) / 400).Begin();
            if (Math.Abs(_projection.LocalOffsetY) < _height * 0.3)
            {
                CreateStoryboard(
                    "LocalOffsetY",
                    _projection.LocalOffsetY,
                    0,
                    Math.Abs(_projection.LocalOffsetY) / 4500).Begin();
            }
            else
            {
                var storyboard = CreateStoryboard(
                    "LocalOffsetY",
                    _projection.LocalOffsetY,
                    _height * Math.Sign(_projection.LocalOffsetY),
                    Math.Abs(_projection.LocalOffsetY) / 4500);
                storyboard.Completed += OnFlashcardHidden;
                storyboard.Begin();
            }
        }

        public void AnimateNext()
        {
            var storyboard = CreateStoryboard("LocalOffsetY", 0, _height, (double)_height / 4500);
            storyboard.Completed += OnFlashcardHidden;
            storyboard.Begin();
        }

        private void OnFlashcardHidden(object sender, EventArgs e)
        {
            _projection.LocalOffsetY = -_projection.LocalOffsetY;
            LoadFlashcard();
            CreateStoryboard("LocalOffsetY", _projection.LocalOffsetY, 0, Math.Abs(_projection.LocalOffsetY) / 4500)
                .Begin();
        }

        public void LoadFlashcard()
        {
            var flashcard = _listener.FetchNextFlashcard();
            if (flashcard.FirstPageType == Flashcard.TypeText)
            {
                _painter.Text1 = flashcard.FirstPageText;
            }
            else
            {
                _painter.Bitmap1 = PictureDecoder.DecodeJpeg(new MemoryStream(flashcard.FirstPageBitmap));
            }
            if (flashcard.SecondPageType == Flashcard.TypeText)
            {
                _painter.Text2 = flashcard.SecondPageText;
            }
            else
            {
                _painter.Bitmap2 = PictureDecoder.DecodeJpeg(new MemoryStream(flashcard.SecondPageBitmap));
            }
            _painter.Repaint();
            if (_projection.RotationY % 360 == 0) return;
            _projection.RotationY = 0;
            UpdateAngle(0);
        }

        private Storyboard CreateStoryboard(string propertyName, double from, double to, double time)
        {
            var sb = new Storyboard();
            var animation = new DoubleAnimation
                {
                    From = @from,
                    To = to,
                    Duration = new Duration(TimeSpan.FromSeconds(time))
                };

            Storyboard.SetTarget(animation, _projection);
            Storyboard.SetTargetProperty(animation, new PropertyPath(propertyName));

            sb.Children.Add(animation);

            return sb;
        }
    }

    public interface IAnimatorListener
    {
        Flashcard FetchNextFlashcard();
    }
}