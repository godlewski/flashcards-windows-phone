﻿using System;
using System.Windows;
using System.Windows.Threading;
using Flashcards.net;
using Microsoft.Phone.Shell;

namespace Flashcards
{
    public class ServerResponseHelper
    {
        private readonly ProgressIndicator _progress;
        private readonly Dispatcher _dispatcher;
        private int _loginActionId;

        public ServerResponseHelper(ProgressIndicator progress, Dispatcher dispatcher)
        {
            _progress = progress;
            _dispatcher = dispatcher;
        }

        public void ServerError(int actionId, ServerErrorType type)
        {
            switch (type)
            {
                case ServerErrorType.ConnectionError:
                    _dispatcher.BeginInvoke(() =>
                    {
                        _progress.IsVisible = false;
                        MessageBox.Show("błąd komunikacji");
                    });
                    break;
                case ServerErrorType.SessionExpired:
                    if (actionId == _loginActionId)
                    {
                        _dispatcher.BeginInvoke(() =>
                        {
                            _progress.IsVisible = false;
                            MessageBox.Show("nastąpił błąd");
                        });
                    }
                    else
                    {
                        _loginActionId = GlobalState.ServerConnector.Login(new LoginData
                        {
                            Login = GlobalState.UserInfo.Login,
                            Password = GlobalState.UserInfo.Password
                        });
                    }
                    break;
                default:
                    _dispatcher.BeginInvoke(() =>
                    {
                        _progress.IsVisible = false;
                        MessageBox.Show("nastąpił błąd");
                    });
                    break;
            }
        }

        public bool LoginSuccess(int actionId, object response, Action action)
        {
            if(actionId == _loginActionId)
            {
                GlobalState.UserInfo.Session = response as string;
                _dispatcher.BeginInvoke(action);
                return true;
            }
            return false;
        }
    }
}
