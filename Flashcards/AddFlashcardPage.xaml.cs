﻿using System;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Flashcards.logic;
using Flashcards.logic.data;
using Flashcards.logic.data.types;
using Flashcards.net;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;

namespace Flashcards
{
    public partial class AddFlashcardPage
    {
        private readonly ApplicationBarManager _appBarMgr;
        private readonly Flashcard _flashcard;
        private readonly string _headerCategoriesText;
        private readonly string _headerFrontPageText;
        private Category _category;
        private int _pageNr;
        private readonly bool _edit;

        public AddFlashcardPage()
        {
            InitializeComponent();

            Categories.ItemsSource = new ObservableCollection<Category>(DbAdapter.FetchAllCategories(new DbContext()));
            _pageNr = 0;
            _headerCategoriesText = Resources["HeaderCategory"] as string;
            _headerFrontPageText = Resources["HeaderFrontPage"] as string;
            _appBarMgr = new ApplicationBarManager(ApplicationBar);
            _edit = GlobalState.Action == GlobalState.ActionType.EditFlashcard;
            if (!_edit)
            {
                _flashcard = new Flashcard();
            }
            else
            {
                _flashcard = GlobalState.Flashcard;
                CategoryPage.Visibility = Visibility.Collapsed;
                if (_flashcard.FirstPageType == Flashcard.TypeText)
                {
                    TextBoxFirst.Text = _flashcard.FirstPageText;
                }
                else
                {
                    ImageViewFirst.Source = Utils.DecodeBitmap(_flashcard.FirstPageBitmap);
                    TextBoxFirst.Visibility = Visibility.Collapsed;
                }

                if (_flashcard.SecondPageType == Flashcard.TypeText)
                {
                    TextBoxSecond.Text = _flashcard.SecondPageText;
                }
                else
                {
                    ImageViewSecond.Source = Utils.DecodeBitmap(_flashcard.SecondPageBitmap);
                    TextBoxSecond.Visibility = Visibility.Collapsed;
                }
            }

            OnShowPage();
        }

        private void PanorameSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = e.AddedItems[0] as PanoramaItem;
            if (item != null && item.Header.Equals(_headerCategoriesText))
            {
                _pageNr = 2;
            }
            else if (item != null && item.Header.Equals(_headerFrontPageText))
            {
                _pageNr = 0;
            }
            else
            {
                _pageNr = 1;
            }
            OnShowPage();
        }

        private void OnShowPage()
        {
            if (_pageNr == 2)
            {
                _appBarMgr.ShowButtons(new[] {2}, new[] {0, 1});
            }
            else
            {
                var photoButtonIndex = 0;
                var removedPhotoButtonIndex = 1;
                if (_pageNr == 0)
                {
                    if (_flashcard.FirstPageBitmap != null)
                    {
                        removedPhotoButtonIndex = 0;
                        photoButtonIndex = 1;
                    }
                }
                else
                {
                    if (_flashcard.SecondPageBitmap != null)
                    {
                        removedPhotoButtonIndex = 0;
                        photoButtonIndex = 1;
                    }
                }
                _appBarMgr.ShowButtons(new[] {photoButtonIndex}, new[] {removedPhotoButtonIndex, 2});
            }
        }

        private void SetPhotoClick(object sender, EventArgs e)
        {
            var photo = new PhotoChooserTask {ShowCamera = true};
            photo.Completed += PhotoChooseCompleted;
            photo.Show();
        }

        public void PhotoChooseCompleted(object sender, PhotoResult result)
        {
            if (result.TaskResult == TaskResult.OK && _pageNr < 2)
            {
                var textBox = _pageNr == 0 ? TextBoxFirst : TextBoxSecond;
                var image = _pageNr == 0 ? ImageViewFirst : ImageViewSecond;
                textBox.Visibility = Visibility.Collapsed;
                image.Visibility = Visibility.Visible;

                var bitmap = Utils.DecodeBitmap(result.ChosenPhoto);
                image.Source = bitmap;
                using (var stream = new MemoryStream())
                {
                    bitmap.SaveJpeg(stream, bitmap.PixelWidth, bitmap.PixelHeight, 0, 95);
                    if (_pageNr == 0)
                    {
                        _flashcard.FirstPageBitmap = stream.ToArray();
                    }
                    else
                    {
                        _flashcard.SecondPageBitmap = stream.ToArray();
                    }
                }
                OnShowPage();
            }
        }

        private void RemovePhotoClick(object sender, EventArgs e)
        {
            var textBox = _pageNr == 0 ? TextBoxFirst : TextBoxSecond;
            var image = _pageNr == 0 ? ImageViewFirst : ImageViewSecond;
            image.Visibility = Visibility.Collapsed;
            textBox.Visibility = Visibility.Visible;
            if (_pageNr == 0)
            {
                _flashcard.FirstPageBitmap = null;
            }
            else
            {
                _flashcard.SecondPageBitmap = null;
            }
            OnShowPage();
        }

        private void AddCategoryClick(object sender, EventArgs e)
        {
            CategoryUiHelper.AddCategoryClick(Categories);
        }

        private void CategoriesListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                _category = e.AddedItems[0] as Category;
            }
            else
            {
                _category = null;
            }
        }

        private void SaveClick(object sender, EventArgs e)
        {
            if (GlobalState.Action == GlobalState.ActionType.AddFlashcard)
            {
                if (_category == null)
                {
                    MessageBox.Show("wybierz kategorię");
                }
                else if (!ValidPage(_flashcard.FirstPageType, _flashcard.FirstPageText, _flashcard.FirstPageBitmap))
                {
                    MessageBox.Show("Wypełnij przednią stronę");
                }
                else if (!ValidPage(_flashcard.SecondPageType, _flashcard.SecondPageText, _flashcard.SecondPageBitmap))
                {
                    MessageBox.Show("Wypełnił tylną stronę");
                }
                else
                {
                    _flashcard.CatId = _category.CategoryId;
                    AppLogic.Instance.AddFlashcard(_flashcard);
                    NavigationService.GoBack();
                }
            }
            else
            {
                if (!ValidPage(_flashcard.FirstPageType, _flashcard.FirstPageText, _flashcard.FirstPageBitmap))
                {
                    MessageBox.Show("Wypełnij przednią stronę");
                }
                else if (!ValidPage(_flashcard.SecondPageType, _flashcard.SecondPageText, _flashcard.SecondPageBitmap))
                {
                    MessageBox.Show("Wypełnił tylną stronę");
                }
                else
                {
                    AppLogic.Instance.EditFlashcard(_flashcard);
                    NavigationService.GoBack();
                }
            }
        }

        private static bool ValidPage(int pageType, string pageText, Binary pagePicture)
        {
            switch (pageType)
            {
                case Flashcard.TypeText:
                    return pageText != null && pageText.Trim().Length > 0;

                case Flashcard.TypeBitmap:
                    return pagePicture != null;
            }
            return false;
        }

        private void BackTextChanged(object sender, TextChangedEventArgs e)
        {
            string text = TextBoxSecond.Text;
            text = text.Trim();
            if (text.Length > 0)
            {
                _flashcard.SecondPageText = text;
            }
        }

        private void FrontTextChanged(object sender, TextChangedEventArgs e)
        {
            string text = TextBoxFirst.Text;
            text = text.Trim();
            if (text.Length > 0)
            {
                _flashcard.FirstPageText = text;
            }
        }

        private void RenameCategoryClick(object sender, RoutedEventArgs e)
        {
            CategoryUiHelper.RenameCategoryClick(sender as MenuItem);
        }

        private void RemoveCategoryClick(object sender, RoutedEventArgs e)
        {
            CategoryUiHelper.RemoveCategoryClick(sender as MenuItem,
                                                 Categories.ItemsSource as ObservableCollection<Category>);
        }
    }
}