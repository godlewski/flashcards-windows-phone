﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Windows.Input;
using Coding4Fun.Phone.Controls;
using Flashcards.logic.data;
using Flashcards.logic.data.types;
using Flashcards.net;
using Flashcards.types;
using Microsoft.Phone.Shell;

namespace Flashcards
{
    public partial class DownloadListPage : IServerConnectorListener
    {
        private readonly object _monitor = new object();

        private readonly ObservableCollection<Category> _categories;
        private readonly ApplicationBarManager _appBarMgr;
        private DownloadedCategory _selectedCategory;
        private int _retrieveActionId;
        private int _loginActionId;
        private int _downloadCategoryActionId;
        private int _lastActionId;
        private string _name;
        private DownloadCategoryResponseData _downloadedCategory;
        private readonly Dictionary<int, string> _firstPages;
        private readonly Dictionary<int, string> _secondPages;
        private readonly Dictionary<int, byte[]> _firstPageImages;
        private readonly Dictionary<int, byte[]> _secondPageImages;

        public DownloadListPage()
        {
            InitializeComponent();
            _firstPages = new Dictionary<int, string>();
            _secondPages = new Dictionary<int, string>();
            _firstPageImages = new Dictionary<int, byte[]>();
            _secondPageImages = new Dictionary<int, byte[]>();
            GlobalState.SelectedCategory = null;
            _appBarMgr = new ApplicationBarManager(ApplicationBar);
            _categories = new ObservableCollection<Category>();
            DownloadsList.ItemsSource = _categories;
            RetrieveCategories(0, 50);
        }

        private void RetrieveCategories(int position, int count)
        {
            Progress.IsVisible = true;
            _lastActionId =
                _retrieveActionId = GlobalState.ServerConnector.RetrieveCategories(new RetrieveCategoriesData
                    {
                        Login = GlobalState.UserInfo.Login,
                        SessionId = GlobalState.UserInfo.Session,
                        Position = position,
                        Size = count
                    });
        }

        private void DownloadClick(object sender, EventArgs e)
        {
            _name = _selectedCategory.Name;
            DoDownloadClick();
        }

        private void DoDownloadClick()
        {
            var db = new DbContext();
            if (DbAdapter.CategoryExists(db, _name))
            {
                var prompt = new InputPrompt
                    {
                        Title = "pobieranie",
                        Message = "posiadasz już kategorię o tej nazwie. Zaproponuj inną nazwę",
                        InputScope =
                            new InputScope {Names = {new InputScopeName {NameValue = InputScopeNameValue.Text}}}
                    };
                prompt.Completed += (senderObject, resultArgs) =>
                    {
                        if (resultArgs.Result == null) return;
                        var result = resultArgs.Result.Trim();
                        if (result.Length == 0) return;
                        _name = result;
                        DoDownloadClick();
                    };
                prompt.Show();
            }
            else
            {
                Dispatcher.BeginInvoke(() => { Progress.IsVisible = true; });
                _lastActionId = _downloadCategoryActionId =
                                GlobalState.ServerConnector.DownloadCategory(new DownloadCategoryData
                                    {
                                        Id = _selectedCategory.ServerCategoryId,
                                        Login = GlobalState.UserInfo.Login,
                                        SessionId = GlobalState.UserInfo.Session
                                    });
            }
        }

        private void ListSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var category = e.AddedItems[0] as DownloadedCategory;
            _selectedCategory = category;
            if (category != null && category.Downloaded)
            {
                _appBarMgr.ShowButtons(new int[0], new[] {0, 1});
                ApplicationBar.Mode = ApplicationBarMode.Minimized;
            }
            else if (category != null && category.DownloadingProgress < 0)
            {
                _appBarMgr.ShowButtons(new[] {0}, new[] {1});
                ApplicationBar.Mode = ApplicationBarMode.Default;
            }
            else
            {
                _appBarMgr.ShowButtons(new[] {1}, new[] {0});
                ApplicationBar.Mode = ApplicationBarMode.Default;
            }
        }

        public void ServerError(int actionId, ServerErrorType type)
        {
            if (actionId == _retrieveActionId)
            {
                switch (type)
                {
                    case ServerErrorType.ConnectionError:
                        Dispatcher.BeginInvoke(() => { Progress.IsVisible = false; });
                        break;
                    case ServerErrorType.SessionExpired:
                        _loginActionId = GlobalState.ServerConnector.Login(new LoginData
                            {
                                Login = GlobalState.UserInfo.Login,
                                Password = GlobalState.UserInfo.Password
                            });
                        break;
                }
            }
            else if (actionId == _loginActionId)
            {
                Dispatcher.BeginInvoke(() => { Progress.IsVisible = false; });
            }
        }

        public void ServerSuccess(int actionId, object response)
        {
            if (actionId == _loginActionId)
            {
                GlobalState.UserInfo.Session = response as string;
                Dispatcher.BeginInvoke(() => RetrieveCategories(0, 50));
            }
            else if (_lastActionId == _retrieveActionId)
            {
                Dispatcher.BeginInvoke(() =>
                    {
                        var result = response as IList<DownloadedCategory>;
                        if (result != null)
                            DownloadsList.ItemsSource = new ObservableCollection<DownloadedCategory>(result);
                        Progress.IsVisible = false;
                    });
            }
            else if (_lastActionId == _downloadCategoryActionId)
            {
                _downloadedCategory = response as DownloadCategoryResponseData;
                Dispatcher.BeginInvoke(FinishDownloading);
            }
        }

        private void FinishDownloading()
        {
            for (var i = 0; i < _downloadedCategory.Flashcards.Count; i++)
            {
                var flashcard = _downloadedCategory.Flashcards[i];
                if (flashcard.FirstPageType == DownloadFlashcardResponseData.Type.Image)
                    _firstPages.Add(i, flashcard.FirstPage);
                if (flashcard.SecondPageType == DownloadFlashcardResponseData.Type.Image)
                    _secondPages.Add(i, flashcard.SecondPage);
            }
            if (_firstPages.Count == 0 && _secondPages.Count == 0)
            {
                _selectedCategory.DownloadingProgress = 100;
                AddDownloadedCategory();
            }
            else
            {
                DownloadImages();
            }
        }

        private void DownloadImages()
        {
            try
            {
                foreach (var key in _firstPages.Keys)
                {
                    var webClient = new WebClient();
                    var flashcardId = key;
                    webClient.OpenReadCompleted += (sender, e) =>
                        {
                            lock (_monitor)
                            {
                                var data = ReadImageResponse(e);
                                _firstPageImages.Add(flashcardId, data);
                                _firstPages.Remove(flashcardId);
                                if (_firstPages.Count + _secondPages.Count == 0)
                                {
                                    Dispatcher.BeginInvoke(AddDownloadedCategory);
                                }
                            }
                        };
                    webClient.OpenReadAsync(new Uri(_firstPages[key], UriKind.Absolute));
                }

                foreach (var key in _secondPages.Keys)
                {
                    var webClient = new WebClient();
                    var flashcardId = key;
                    webClient.OpenReadCompleted += (sender, e) =>
                        {
                            lock (_monitor)
                            {
                                var data = ReadImageResponse(e);
                                _secondPageImages.Add(flashcardId, data);
                                _secondPages.Remove(flashcardId);
                                if (_firstPages.Count + _secondPages.Count == 0)
                                {
                                    Dispatcher.BeginInvoke(AddDownloadedCategory);
                                }
                            }
                        };
                    webClient.OpenReadAsync(new Uri(_secondPages[key], UriKind.Absolute));
                }
            }
            catch (Exception)
            {
                CleanUp();
            }
        }

        private void AddDownloadedCategory()
        {
            var db = new DbContext();
            var category = new Category
                {
                    ServerCategoryId = _downloadedCategory.Id,
                    Name = _name,
                    Downloaded = true,
                    Author = "ktoś"
                };
            DbAdapter.AddCategory(db, category);
            var i = 0;

            foreach (var f in _downloadedCategory.Flashcards)
            {
                var flashcard = new Flashcard();

                if (f.FirstPageType == DownloadFlashcardResponseData.Type.Text)
                    flashcard.FirstPageText = f.FirstPage;
                else
                    flashcard.FirstPageBitmap = _firstPageImages[i];

                if (f.SecondPageType == DownloadFlashcardResponseData.Type.Text)
                    flashcard.SecondPageText = f.SecondPage;
                else
                    flashcard.SecondPageBitmap = _secondPageImages[i];

                flashcard.CatId = category.CategoryId;
                DbAdapter.AddFlashcard(db, flashcard);
                i++;
            }
            CleanUp();
            Progress.IsVisible = false;
        }

        private byte[] ReadImageResponse(OpenReadCompletedEventArgs e)
        {
            if (e.Error == null && !e.Cancelled)
            {
                try
                {
                    using (var ms = new MemoryStream())
                    {
                        e.Result.CopyTo(ms);
                        return ms.ToArray();
                    }
                }
                catch (Exception)
                {
                    CleanUp();
                }
            }
            else
            {
                CleanUp();
            }
            return null;
        }

        private void CleanUp()
        {
            _firstPages.Clear();
            _secondPages.Clear();
            _firstPageImages.Clear();
            _secondPageImages.Clear();
        }

        private void VotesClick(object sender, EventArgs e)
        {
            if (_selectedCategory == null) return;
            GlobalState.SelectedCategory = _selectedCategory;
            NavigationService.Navigate(new Uri("/CommentsPage.xaml", UriKind.Relative));
        }
    }
}