﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using Flashcards.logic.data.types;
using Flashcards.net;

namespace Flashcards
{
    public partial class AuthorizationPage : IServerConnectorListener
    {
        private const string FillDataMsg = "Proszę wypełnić wszystkie pola";
        private const string InvalidLoginOrPasswordMsg = "Niepoprawny login lub hasło";
        private const string ConnectionErrorMsg = "Nastąpił problem z połączeniem";
        private const string PasswordsNotEqualMsg = "Hasła się nie zgadzają";
        private const string UserAlreadyExistsMsg = "Użytkownik o takim loginie lub e-mailu już istnieje";

        private int _loginActionId;
        private int _registerActionId;
        private LoginData _loginData;

        public AuthorizationPage()
        {
            InitializeComponent();
        }

        private void RegisterClick(object sender, RoutedEventArgs e)
        {
            if (!ParseRegister())
            {
                MessageBox.Show(FillDataMsg);
            }
            else if(RegisterPassword.Password != RegisterPassword2.Password)
            {
                MessageBox.Show(PasswordsNotEqualMsg);
            }
            else
            {
                Register();
            }
        }

        private void LoginClick(object sender, RoutedEventArgs e)
        {
            if (ParseLogin())
            {
                Login();
            }
            else
            {
                MessageBox.Show(FillDataMsg);
            }
        }

        private bool ParseRegister()
        {
            return !EmptyField(RegisterEmail) && !EmptyField(RegisterLogin) && !EmptyField(RegisterPassword) &&
                   !EmptyField(RegisterPassword2) && IsEmail(RegisterEmail.Text);
        }

        private bool ParseLogin()
        {
            return !EmptyField(LoginLogin) && !EmptyField(LoginPassword);
        }

        private void Login()
        {
            Progress.IsVisible = true;
            _loginActionId = GlobalState.ServerConnector.Login(_loginData = new LoginData
                {
                    Login = LoginLogin.Text.Trim(),
                    Password = LoginPassword.Password.Trim()
                });
        }

        private void Register()
        {
            Progress.IsVisible = true;
            var registerData = new RegisterData
                {
                    Email = RegisterEmail.Text.Trim(),
                    Login = RegisterLogin.Text.Trim(),
                    Password = RegisterPassword.Password
                };
            _loginData = new LoginData
                {
                    Login = registerData.Login,
                    Password = registerData.Password
                };
            _registerActionId = GlobalState.ServerConnector.Register(registerData);
        }

        private static bool EmptyField(TextBox field)
        {
            return field.Text.Trim().Length == 0;
        }

        private static bool EmptyField(PasswordBox field)
        {
            return field.Password.Trim().Length == 0;
        }

        private static bool IsEmail(string email)
        {
            var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            var match = regex.Match(email);
            return match.Success;
        }


        public void ServerError(int actionId, ServerErrorType type)
        {
            Dispatcher.BeginInvoke(() => { Progress.IsVisible = false; });
            if (actionId == _registerActionId)
            {
                switch (type)
                {
                    case ServerErrorType.ConnectionError:
                        ShowMsg(ConnectionErrorMsg);
                        break;

                    case ServerErrorType.UserAlreadyExists:
                        ShowMsg(UserAlreadyExistsMsg);
                        break;
                }
            }
            else if (actionId == _loginActionId)
            {
                switch (type)
                {
                    case ServerErrorType.ConnectionError:
                        ShowMsg(ConnectionErrorMsg);
                        break;

                    case ServerErrorType.InvalidLoginOrPassword:
                        ShowMsg(InvalidLoginOrPasswordMsg);
                        break;
                }
            }
        }

        public void ServerSuccess(int actionId, object response)
        {
            if (actionId == _loginActionId)
            {
                Dispatcher.BeginInvoke(() => { Progress.IsVisible = false; });
                var userInfo = new UserInfo
                    {
                        Login = _loginData.Login,
                        Password = _loginData.Password,
                        Session = response as string
                    };
                GlobalState.UserInfo = userInfo;
                Dispatcher.BeginInvoke(() => NavigationService.Navigate(new Uri(GlobalState.AfterLoginPage, UriKind.Relative)));
            }
            else if (actionId == _registerActionId)
            {
                _loginActionId = GlobalState.ServerConnector.Login(_loginData);
            }
        }

        private void ShowMsg(string msg)
        {
            Dispatcher.BeginInvoke(() => MessageBox.Show(msg));
        }
    }
}