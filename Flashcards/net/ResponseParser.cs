﻿using System.Collections.Generic;
using System.Linq;
using Flashcards.types;
using Newtonsoft.Json.Linq;

namespace Flashcards.net
{
    public class ResponseParser
    {
        public static IList<DownloadedCategory> ParseRetrieveCategories(string json)
        {
            return JArray.Parse(json).Select(jToken => jToken.ToObject<DownloadedCategoryItemResponseData>()).
                Select(item => new DownloadedCategory
                    {
                        Author = "nieznany",
                        Vote = (int) item.MeanVote,
                        Downloaded = false,
                        ServerCategoryId = item.Id,
                        Count = item.Count,
                        DownloadingProgress = -1,
                        Name = item.Name,
                        Downloads = item.Downloads,
                        Votes = item.Votes
                    }).ToList();
        }

        public static IList<VoteResponseData> ParseRetrieveVotes(string json)
        {
            return JArray.Parse(json).Select(jToken => jToken.ToObject<VoteResponseData>()).ToList();
        }

        public static DownloadCategoryResponseData ParseDownloadCategory(string json)
        {
            return JObject.Parse(json).ToObject<DownloadCategoryResponseData>();
        }
    }
}