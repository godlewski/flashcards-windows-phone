﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Flashcards.logic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace Flashcards.net
{
    public class ServerConnector
    {
        private const string ServerAddress = "http://luski.linuxpl.info/api/";

        #region Actions' Names

        private const string ActionRegister = "register/";
        private const string ActionLogin = "login/";
        private const string ActionCreateCategory = "add_category/";
        private const string ActionRetrieveCategories = "retrieve_categories/";
        private const string ActionDownloadCategory = "download_category/";
        private const string ActionVoteCategory = "vote_category/";
        private const string ActionUploadImage = "upload_file/";
        private const string ActionRetrieveVotes = "retrieve_votes/";

        #endregion

        public enum ActionType
        {
            Register,
            Login,
            CreateCategory,
            VoteCategory,
            DownloadCategory,
            RetrieveCategories,
            UploadImage,
            RetrieveVotes
        }

        private readonly IServerConnectorListener _listener;
        private static int _actionId;
        private readonly Dictionary<int, ActionType> _actions;

        public ServerConnector(IServerConnectorListener listener)
        {
            _listener = listener;
            _actions = new Dictionary<int, ActionType>();
            _actionId = 10;
        }

        public int Register(RegisterData registerData)
        {
            var input = CreateInput(registerData);
            return SendPost(input, ActionRegister, ActionType.Register);
        }

        public int Login(LoginData loginData)
        {
            var input = CreateInput(loginData);
            return SendPost(input, ActionLogin, ActionType.Login);
        }

        public int CreateCategory(CreateCategoryData createCategoryData)
        {
            var input = CreateInput(createCategoryData);
            Utils.Log("Send: " + input);
            return SendPost(input, ActionCreateCategory, ActionType.CreateCategory);
        }

        public int RetrieveCategories(RetrieveCategoriesData retrieveCategoriesData)
        {
            var input = CreateInput(retrieveCategoriesData);
            return SendPost(input, ActionRetrieveCategories, ActionType.RetrieveCategories);
        }

        public int RetrieveVotes(RetrieveVotesData retrieveVotesData)
        {
            var input = CreateInput(retrieveVotesData);
            return SendPost(input, ActionRetrieveVotes, ActionType.RetrieveVotes);
        }

        public int VoteCategory(VoteCategoryData voteCategoryData)
        {
            var input = CreateInput(voteCategoryData);
            return SendPost(input, ActionVoteCategory, ActionType.VoteCategory);
        }

        public int DownloadCategory(DownloadCategoryData downloadCategoryData)
        {
            var input = CreateInput(downloadCategoryData);
            return SendPost(input, ActionDownloadCategory, ActionType.DownloadCategory);
        }

        public int UploadImage(byte[] data, UploadFileData uploadFileData)
        {
            var request = new RestRequest(ActionUploadImage, Method.POST);
            request.AddFile("file_field", data, "fileName");
            request.AddParameter("json", JsonConvert.SerializeObject(uploadFileData));
            var actionId = _actionId++;
            request.AddParameter("actionId", actionId);
            var client = new RestClient(ServerAddress);
            client.ExecuteAsync(request, response =>
                {
                    if (response.ErrorException != null)
                    {
                        Utils.Log(response.ErrorMessage);
                        _listener.ServerError(actionId, ServerErrorType.ConnectionError);
                        return;
                    }
                    var jObject = JObject.Parse(response.Content);
                    var result = jObject["result"].ToObject<string>();
                    if (result == "ERROR")
                    {
                        _listener.ServerError(actionId,
                                              jObject["error_code"].ToObject<ServerErrorType>());
                    }
                    else
                    {
                        _listener.ServerSuccess(actionId,
                                                jObject["data"].ToString());
                    }
                });
            return actionId;
        }

        private static string CreateInput(object data)
        {
            return "json=" + JsonConvert.SerializeObject(data);
        }

        #region SendingPost

        private int SendPost(string input, string actionName, ActionType actionType)
        {
            var myUri = new Uri(ServerAddress + actionName);
            var request = (HttpWebRequest) WebRequest.Create(myUri);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            var actionId = _actionId++;
            _actions.Add(actionId, actionType);
            request.BeginGetRequestStream(SendRequest, new SendPostData
                {
                    ActionId = actionId,
                    Data = input,
                    Request = request
                });
            return actionId;
        }


        private void SendRequest(IAsyncResult ar)
        {
            var state = ar.AsyncState as SendPostData;
            if (state == null) return;
            var stream = state.Request.EndGetRequestStream(ar);
            var bytes = Encoding.UTF8.GetBytes(state.Data);
            stream.Write(bytes, 0, bytes.Length);
            stream.Close();
            state.Request.BeginGetResponse(GetResponse, state);
        }

        private void GetResponse(IAsyncResult ar)
        {
            var state = ar.AsyncState as SendPostData;
            if (state == null) return;
            var actionType = _actions[state.ActionId];
            _actions.Remove(state.ActionId);
            try
            {
                var response = state.Request.EndGetResponse(ar);
                using (var httpWebStreamReader = new StreamReader(response.GetResponseStream()))
                {
                    var jsonResponse = httpWebStreamReader.ReadToEnd();
                    var jObject = JObject.Parse(jsonResponse);
                    var result = jObject["result"].ToObject<string>();
                    if (result == "ERROR")
                    {
                        _listener.ServerError(state.ActionId,
                                              jObject["error_code"].ToObject<ServerErrorType>());
                    }
                    else
                    {
                        switch (actionType)
                        {
                            case ActionType.Register:
                                _listener.ServerSuccess(state.ActionId, null);
                                break;
                            case ActionType.Login:
                                _listener.ServerSuccess(state.ActionId, jObject["data"].ToObject<string>());
                                break;
                            case ActionType.CreateCategory:
                                _listener.ServerSuccess(state.ActionId, null);
                                break;
                            case ActionType.VoteCategory:
                                _listener.ServerSuccess(state.ActionId, null);
                                break;
                            case ActionType.DownloadCategory:
                                _listener.ServerSuccess(state.ActionId, 
                                                        ResponseParser.ParseDownloadCategory(
                                                            jObject["data"].ToString()));
                                break;
                            case ActionType.RetrieveCategories:
                                _listener.ServerSuccess(state.ActionId,
                                                        ResponseParser.ParseRetrieveCategories(
                                                            jObject["data"].ToString()));
                                break;

                            case ActionType.RetrieveVotes:
                                _listener.ServerSuccess(state.ActionId,
                                                        ResponseParser.ParseRetrieveVotes(jObject["data"].ToString()));
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }
            }
            catch (WebException e)
            {
                Utils.Log(e.Message);
                _listener.ServerError(state.ActionId, ServerErrorType.ConnectionError);
            }
        }

        #endregion
    }

    public interface IServerConnectorListener
    {
        void ServerError(int actionId, ServerErrorType type);
        void ServerSuccess(int actionId, object response);
    }

    public enum ServerErrorType
    {
        ConnectionError = 0,
        UserAlreadyExists = 101,
        WrongDataError = 102,
        InvalidLoginOrPassword = 103,
        SessionExpired = 104,
        CategoryAlreadyExists = 105,
        CategoryNotExists = 106
    }
}