﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace Flashcards.net
{
    public class SendPostData
    {
        public HttpWebRequest Request { get; set; }
        public string Data { get; set; }
        public int ActionId { get; set; }
    }

    public class RegisterData
    {
        [JsonProperty(PropertyName = "login")]
        public string Login { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
    }

    public class LoginData
    {
        [JsonProperty(PropertyName = "login")]
        public string Login { get; set; }

        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
    }

    public class CreateCategoryData
    {
        [JsonProperty(PropertyName = "login")]
        public string Login { get; set; }

        [JsonProperty(PropertyName = "session_id")]
        public string SessionId { get; set; }

        [JsonProperty(PropertyName = "category")]
        public CategoryResponseData Category { get; set; }

        [JsonProperty(PropertyName = "flashcards")]
        public IList<FlashcardResponseData> Flashcards { get; set; }

        [JsonProperty(PropertyName = "keywords")]
        public string Keywords { get; set; }
    }

    public class CategoryResponseData
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "language")]
        public string Language { get; set; }

    }

    public class FlashcardResponseData
    {
        public const int TypeText = 1;
        public const int TypeImage = 2;

        [JsonProperty(PropertyName = "first_page")]
        public string FirstPage { get; set; }

        [JsonProperty(PropertyName = "first_page_type")]
        public int FirstPageType { get; set; }

        [JsonProperty(PropertyName = "second_page")]
        public string SecondPage { get; set; }

        [JsonProperty(PropertyName = "second_page_type")]
        public int SecondPageType { get; set; }
    }

    public class RetrieveCategoriesData
    {
        [JsonProperty(PropertyName = "login")]
        public string Login { get; set; }

        [JsonProperty(PropertyName = "session_id")]
        public string SessionId { get; set; }

        [JsonProperty(PropertyName = "size")]
        public int Size { get; set; }

        [JsonProperty(PropertyName = "position")]
        public int Position { get; set; }
    }

    public class DownloadedCategoryItemResponseData
    {
        [JsonProperty(PropertyName = "count")]
        public int Count { get; set; }

        [JsonProperty(PropertyName = "votes")]
        public int Votes { get; set; }

        [JsonProperty(PropertyName = "mean_vote")]
        public Double MeanVote { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "downloads")]
        public int Downloads { get; set; }
    }

    public class UploadFileData
    {
        [JsonProperty(PropertyName = "login")]
        public string Login { get; set; }

        [JsonProperty(PropertyName = "session_id")]
        public string SessionId { get; set; }
    }

    public class RetrieveVotesData
    {
        [JsonProperty(PropertyName = "login")]
        public string Login { get; set; }

        [JsonProperty(PropertyName = "session_id")]
        public string SessionId { get; set; }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
    }

    public class VoteResponseData
    {
        [JsonProperty(PropertyName = "comment")]
        public string Comment { get; set; }

        [JsonProperty(PropertyName = "vote")]
        public int Vote { get; set; }

        [JsonProperty(PropertyName = "user")]
        public string User { get; set; }
    }

    public class VoteCategoryData
    {
        [JsonProperty(PropertyName = "login")]
        public string Login { get; set; }

        [JsonProperty(PropertyName = "session_id")]
        public string SessionId { get; set; }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "vote")]
        public int Vote { get; set; }

        [JsonProperty(PropertyName = "comment")]
        public string Comment { get; set; }
    }

    public class DownloadCategoryData
    {
        [JsonProperty(PropertyName = "login")]
        public string Login { get; set; }

        [JsonProperty(PropertyName = "session_id")]
        public string SessionId { get; set; }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
    }

    public class DownloadCategoryResponseData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<DownloadFlashcardResponseData> Flashcards { get; set; } 
    }

    public class DownloadFlashcardResponseData
    {
        public enum Type
        {
            Text = 1,
            Image = 2
        }

        [JsonProperty(PropertyName = "first_page_type")]
        public Type FirstPageType { get; set; }

        [JsonProperty(PropertyName = "second_page_type")]
        public Type SecondPageType { get; set; }

        [JsonProperty(PropertyName = "first_page")]
        public string FirstPage { get; set; }

        [JsonProperty(PropertyName = "second_page")]
        public string SecondPage { get; set; }
    }

}
