﻿using System;
using System.ComponentModel;
using Flashcards.logic.data.types;

namespace Flashcards.types
{
    public class DownloadedCategory : Category
    {
        public event PropertyChangedEventHandler DownloadingPropertyChanged;

        private int _downloadingProgress;

        public int Vote { get; set; }

        public int Downloads { get; set; }

        public int DownloadingProgress
        {
            get { return _downloadingProgress; }
            set
            {
                if (_downloadingProgress == value) return;
                _downloadingProgress = Math.Min(100, value);
                if (DownloadingPropertyChanged != null)
                    DownloadingPropertyChanged(this, new PropertyChangedEventArgs("DownloadingProgress"));
            }
        }

        public string DownloadingStatus
        {
            get
            {
                var progress = DownloadingProgress;
                if (progress < 0)
                {
                    return "";
                }
                return progress < 100 ? "pobieranie" : "pobrano";
            }
        }

        public bool IsDownloading
        {
            get { return DownloadingProgress >= 0 && DownloadingProgress < 100; }
        }

        public int Votes { get; set; }
    }
}